/******************************************************************************/
/*	                     		Application: RICMessenger Copyright(C)            */
/******************************************************************************/
/*									      	                                                	*/
/*			        Fichier :  index.js  	                                				*/                                       	
/*									                                                      		*/
/*		          Auteurs : REYGROBELLET -	BRES -	PELISSON                    */
/*									    		                                                  */
/*                 Date : 8/04/2019                                           */
/******************************************************************************/

const node = new Ipfs({
  EXPERIMENTAL: {
    pubsub: true
  },
  config: {
    Addresses: {
      Swarm: ['/dns4/ws-star.discovery.libp2p.io/tcp/443/wss/p2p-websocket-star']
    }
  }
})


var currentTab = "Home";
var id;
// once the node is ready
node.once('ready', () => {

  sub(currentTab);
  node.id(function (err, identity) {
    id = identity.id;
    if(localStorage.getItem(id)){
      document.getElementById("usern").innerHTML=localStorage.getItem(id);
      $("#usern").append('<i class="fas fa-pen"</i>');
    }
    else{
      $('#usrmodal').modal('show');
    }
  });

  setInterval(function() {
    refreshUsersList();
    refreshConnectedUser();
  }, 2000);

});

(function($) {

  document.getElementById("msgList-Home").value="";

  $("#message").keyup(function(event) {
    if (event.keyCode === 13) {
      $("#sndMsg").click();
    }
  });
  $("#username").keyup(function(event) {
    if (event.keyCode === 13) {
      $("#saveusrn").click();
    }
  });
  $("#channelname").keyup(function(event) {
    if (event.keyCode === 13) {
      $("#savechnl").click();
    }
  });

  $('#usrmodal').on('shown.bs.modal', function () {
    $('#username').focus();
  });
  $('#chnlmodal').on('shown.bs.modal', function () {
    $('#channelname').focus();
  });
  $('#chnlmodal').on('hidden.bs.modal', function () {
    copyTkButton();
  });
  $('#joinmodal').on('shown.bs.modal', function () {
    $('#channeljnname').focus();
  });

  // Behavior on click on a tab
  // Display the according textarea, remove the notification bullet,
  // refresh the users list
  $("#nav-tabs").on("click", "a", function (e) {
    e.preventDefault();
    currentTab=$(this).attr("aria-label");
    $('#message').focus();
    refreshUsersList();
    $(this).find('span').remove();
    displayFileList(currentTab);
  });

  // same as above but for the side list tab
  $('#list-tab').on('click', "a", function (e) {
    e.preventDefault();
    var chan=$(this).attr('aria-label');
    displayChannel(chan);
  })

  $('#message').focus();
  $('[data-toggle="tooltip"]').tooltip();
  $('.collapse').collapse();

  // On load of the page, restore the previous session with the opened tab, theme
  var tabs=localStorage.getItem("tabs");
  if(tabs!=null){
    var arrTabs=tabs.split(',');
    for (i=0;i<arrTabs.length;i++){
      if(arrTabs[i]!=""){
        var channel=arrTabs[i];
        if(channel.indexOf( "RICM_PRIV-" ) > -1){
          if(localStorage.getItem(channel)!=null){
            var channelAlias=localStorage.getItem(channel);
          }
        }
        else{
          var channelAlias=channel;
        }
        var private=(channel.indexOf( "RICM_PRIV-" ) > -1);
        if(private){
          var shortTok=channel.slice(10);
          $('#list-tab').append('<a class="list-group-item list-group-item-action" id="list-'+channelAlias+'-list" data-toggle="list" href="#'+channel+'" role="tab" aria-controls="'+channelAlias+'" aria-label="'+channel+'"><div> <p class="token">'+channelAlias+' <i class="fas fa-lock"></i></p> <button class="close closeTab" type="button">×</button> <span class="badge badge-success badge-pill">0 Connected</span></div><small>TOKEN : <i>'+shortTok+'</i></small></a>');
        }
        else{
          $('#list-tab').append('<a class="list-group-item list-group-item-action" id="list-'+channelAlias+'-list" data-toggle="list" href="#'+channel+'" role="tab" aria-controls="'+channelAlias+'" aria-label="'+channel+'"><div> <p class="token">'+channelAlias+'</i></p> <button class="close closeTab" type="button">×</button> <span class="badge badge-success badge-pill">0 Connected</span></div></a>');
        }
      }
    }
  }
  else{
    localStorage.setItem("tabs",["Home"]);
    $('#list-tab').append('<a class="list-group-item list-group-item-action active" id="list-Home-list" data-toggle="list" href="#Home" role="tab" aria-controls="Home" aria-label="Home">Home<button class="close closeTab" type="button">×</button><span class="badge badge-success badge-pill">0 Connected</span></a>');
  }

  var theme = localStorage.getItem("theme");
  if(theme=="dark"){
    switchDarkMode();
  }

})(jQuery);


// receiveMsg() is a handler for every received messages on a subscribed channel
//
// we check what kind of informationhas been sent and then we proccess the message with the
// according function
// this allows the code to be easily improved by adding new type of data with the according function
// to handle it
const receiveMsg = (data) => {
  var crypted=data.data.toString();
  var chl=data.topicIDs[0];
  var str=decryptMsg(crypted,chl);
  var strArr=str.split(";");
  var type=strArr[0];
  switch(type) {
    case "[0001]":          //classic text message
      textReceived(data);
      break;
    case "[0010]":          //file received
      imageReceived(data);
      break;
    case "[0011]":          //username changed
      newUserName(data);
    default:
  }
}

// textReceived() is a function to process the message if it's plain text as a text message
// data is an implicit argument passed by pubsub.subscribe in the sub() function.
// it is an object containing many information such as the channel, the peed id, the message and more
//
// For every received msg (data), we check : the channel, the sender, and the message.
// We link the peer ID to the username passed through the message.
// The message is displayed in the right textarea, and the notification bullet is updated
// if it's not the active tab.
function textReceived(data){
  var crypted=data.data.toString();
  var chl=data.topicIDs[0];
  var str=decryptMsg(crypted,chl);
  var strArr = str.split(";");
  var usrn=strArr[1];
  var msg=strArr[2];
  var peer=data.from;
  var chan=data.topicIDs[0];

  //We match the channel name with the alias set by the user to display in the tab
  if(chl.indexOf( "RICM_PRIV-" ) > -1){
    if(localStorage.getItem(chl)!=null){
      chl=localStorage.getItem(chan);
    }
  }
  console.log('Nouveau message on channel : '+ chan);
  console.log('From : '+peer+' ; ('+usrn+')');
  console.log('msg : '+ msg);

  printChat(chan,usrn+' : '+msg);

  localStorage.setItem(peer, usrn);
}


// When someone set his username of change it, the switch in receiveMsg() call this func to
// display a message in the chat and save the username according the the peer ID (sender)
function newUserName(data){
  var crypted = data.data.toString();
  var chl = data.topicIDs[0];
  var str = decryptMsg(crypted,chl);
  var peer = data.from;
  var tab = str.split(";");
  if(tab[1]!=""){
    var msg= "-- "+tab[1]+" renamed in "+tab[2]+" --";
  }
  else{
    var msg= "-- New user : "+tab[2]+" --";
  }
  localStorage.setItem(peer, tab[2]);
  printChat(chl,msg);
}


// function to print any message in any channel to make it easier and cleaner
// when we need to print anything (message, username edit, future feature) in
// the chatbox
// handle the notification bullet also.
function printChat(chan,msg){
  var textarea = document.getElementById('msgList-'+chan);
  textarea.value+=msg+"\n";
  textarea.scrollTop = (textarea.scrollHeight-100);

  var chl=chan;
  if(chan.indexOf( "RICM_PRIV-" ) > -1){
    if(localStorage.getItem(chan)!=null){
      chl=localStorage.getItem(chan);
    }
  }
  if(chan!=currentTab){
    if(!$("#nav-"+chl+"-tab[aria-label='"+chan+"'] span").length){
      $("#nav-"+chl+"-tab[aria-label='"+chan+"'] button").before('<span class="badge badge-primary badge-pill">1</span>');
    }
    else{
      var nb=$("#nav-"+chl+"-tab[aria-label='"+chan+"'] span").text();
      $("#nav-"+chl+"-tab[aria-label='"+chan+"'] span").text(Number(nb)+1);
    }
  }
}


// sub() is a function to subscribe to a specific channel
// channel is an argument passed as a string to specify to which channel sub
function sub(channel){
  node.pubsub.subscribe(channel,receiveMsg);
}


// pub() is a function to publish a message in a specific channel
// msg is an argument passed as a string containing the message to send
// channel is an argument passed as a string containing the channel to publish into
//
// the message is crypted with the func chiffrerMsg() and the transformed into a Buffer
// the buffer is send to the channel with the func pubsub.publish()
function pub(msg, channel){
  var msgChiffrer = chiffrerMsg(msg,channel);
  const buff = node.types.Buffer.from(msgChiffrer);
  node.pubsub.publish(channel, buff);
}

// sendMsg() is called onClick() of the send button
// it gets the username of the user and the message to send
// if the message is not empty, it's send to the pub() function just above
function sendMsg(){
  un=$("#usern").text();
  ms=$("#message").val();
  if(ms!=""){
    msg="[0001];"+un+";"+ms;
    $("#message").val("");
    pub(msg, getCurrentChnl());
  }
}


// function to change the username of the client
// a message is send to the current channel to notify everyone of the change
// the username is saved to the localStorage to recover it the next session
function setusrn(){
  prev=$("#usern").text();
  usrn=$("#username").val();
  if(usrn!=""){
    $("#usern").text(usrn);
    $("#username").val("");
    $("#usern").append('<i class="fas fa-pen"</i>');
    (function($) {
      $('#usrmodal').modal('hide');
      $('#message').focus();
    })(jQuery);
    var msg="[0011];"+prev+";"+usrn;
    $("#nav-tabs a").each(function() {
      var chan = $(this).attr('aria-label');
      pub(msg, chan);
    });
    localStorage.setItem(id, usrn);
  }
}

// return the active tab AKA the current channel
function getCurrentChnl(){
  return $("#nav-tabs").find("[aria-selected='true']").attr("aria-label");
}

// displayChannel() is a function to update the frontend and display the chqt of a channel
// if the tab is not already open, then add it the the interface
// when a new channel is opened, the channel id is saved to the localStorage and added to the left column
// to retreive it for the next session
function displayChannel(channel){
  var private=(channel.indexOf( "RICM_PRIV-" ) > -1);
  var chnlAlias;
  if(private){
    chnlAlias=localStorage.getItem(channel);
  }
  else{
    chnlAlias=channel;
  }
  if(chnlAlias!=""){
    if(!$("#nav-"+chnlAlias+"-tab[aria-label='"+channel+"']").length){
      $('#nav-tabContent').append( "<div class='tab-pane fade' id='"+channel+"' role='tabpanel' aria-labelledby='nav-"+channel+"-tab' aria-label='"+channel+"'> <textarea id='msgList-"+channel+"' rows='10' class='form-control' readonly></textarea> </div>" );
      $("#nav-tabs").append("<a class='nav-item nav-link' id='nav-"+chnlAlias+"-tab' data-toggle='tab' href='#"+channel+"' role='tab' aria-controls='"+chnlAlias+"' aria-selected='false' aria-label='"+channel+"'>"+chnlAlias+" <button class='close closeTab' type='button' >×</button></a>");
      $("#msgList-"+chnlAlias).val("");
      sub(channel);

      var tabs=localStorage.getItem("tabs");
      if(tabs!=null){
        var arrTabs=tabs.split(',');
        var found=false;
        for (i=0;i<arrTabs.length;i++){
          if(arrTabs[i]==channel){
            found=true;
          }
        }
        if(!found){
          arrTabs[arrTabs.length]=channel;
        }
        var newArr=arrTabs.join();
      }
      else{
        var newArr=[channel];
      }
      // console.log(newArr);
      localStorage.setItem("tabs", newArr);
    }
    var shortTok=channel.slice(10);
    if(!$('#list-'+chnlAlias+'-list[aria-label="'+channel+'"]').length){
      if(private){
        $('#list-tab').append('<a class="list-group-item list-group-item-action" id="list-'+chnlAlias+'-list" data-toggle="list" href="#'+chnlAlias+'" role="tab" aria-controls="'+chnlAlias+'" aria-label="'+channel+'"><div> <p class="token">'+chnlAlias+' <i class="fas fa-lock"></i></p> <button class="close closeTab" type="button">×</button> <span class="badge badge-success badge-pill">0 Connected</span></div><small>TOKEN : <i>'+shortTok+'</i></small></a>');
      }
      else{
        $('#list-tab').append('<a class="list-group-item list-group-item-action" id="list-'+chnlAlias+'-list" data-toggle="list" href="#'+chnlAlias+'" role="tab" aria-controls="'+chnlAlias+'" aria-label="'+channel+'"><div> <p class="token">'+chnlAlias+'</i></p> <button class="close closeTab" type="button">×</button> <span class="badge badge-success badge-pill">0 Connected</span></div></a>');
      }
    }
    $("#channelname").val("");
    if(private){
      $("#tokenShow").val(shortTok);
      $("#createC").hide();
      $("#copyToken").css('display', 'flex');
      $("#CreateCnlModalTitle").text("Your Token");
    }
    else{
      $('#chnlmodal').modal('hide');
    }
    $("#nav-"+chnlAlias+"-tab[aria-label='"+channel+"']").trigger("click");
    registerCloseEvent();
  }
}

// createchnl() is a function called onClick() of the Create button
// if the channel created has to be Private, then we generate a unique token
// The private channel ID will be a string "RICM_PRIV-" concat with the unique token
// The alias set by the user is saved in the localStorage for the next session
// If the channel is public, then the channel ID is just the alias set by the user
function createchnl(){
  if($('#private').is(':checked')){
    var tok=genToken();
    var channel="RICM_PRIV-"+tok;
    var alias=$("#channelname").val();
    $('#private').prop( "checked", false );
    localStorage.setItem(channel,alias);
    displayChannel(channel);
  }
  else{
    var channel=$("#channelname").val();
    displayChannel(channel);
  }
}

// genToken() generate a unique token of 8 characters using uppercase, lowercase and numbers.
// it returns the unqiue string
function genToken(){
  var chars = "0123456789ABCDEFGHJKLMNOPQRSTUVWXTZabcdefghikmnopqrstuvwxyz";
  var string_length = 8;
  var randomstring = '';
  for (var i=0; i<string_length; i++) {
    var rnum = Math.floor(Math.random() * chars.length);
    randomstring += chars.substring(rnum,rnum+1);
  }
  return randomstring;
}

// Show the modal to change the username
function editun(){
  (function($) {
    $('#usrmodal').modal('show');
  })(jQuery);
}

// Show the modal to create a new channel
function crtchnl(){
  (function($) {
    $('#chnlmodal').modal('show');
  })(jQuery);
}

// Show the modal to join a new channel
function opencnlmodal(){
  (function($) {
    $('#joinmodal').modal('show');
  })(jQuery);
}

// joinchnl() is a function to join the channel entered by the user
// it can be either public or private
// the channel name is passed to displayChannel() to update the front
// and subscribe to the channel
function joinchnl(){
  var token = $("#token").val();
  var alias = $("#channeljnname").val();
  var channel="RICM_PRIV-"+token;
  if(token!=""){
    localStorage.setItem(channel,alias);
    displayChannel(channel);
  }
  else{
    displayChannel(alias);
  }
  $("#token").val("");
  $("#channeljnname").val("");
  $('#joinmodal').modal('hide');
}


// registerCloseEvent() function containing handler for what to do when the close button
// is clicked on a tab or on the left column
// it either close the tab and unsub the channel
// or remove the channel from the localStorage to not load it the next session
function registerCloseEvent() {
  $("#nav-tabs .closeTab").click(function () {
    var tabContentId = $(this).parent().attr("href");
    var chl= $(this).parent().attr("aria-label");
    $(this).parent().remove(); //remove li of tab
    $(tabContentId).remove(); //remove respective tab content
    $('#nav-tabs a:last').tab('show'); // Select first tab
    currentTab=$('#nav-tabs a:last').attr("aria-label");
    node.pubsub.unsubscribe(chl,receiveMsg);
  });
  $("#list-tab .closeTab").click(function () {
    var chl= $(this).parent().parent().attr("aria-label");
    console.log(chl);
    var tabs=localStorage.getItem("tabs");
    var arrTabs=tabs.split(',');
    for (i=0;i<arrTabs.length;i++){
      if(arrTabs[i]==chl){
        arrTabs.splice(i,1);
      }
    }
    var newArr=arrTabs.join();
    console.log(newArr);
    localStorage.setItem("tabs", newArr);
    $(this).parent().parent().remove();
  });
}

// refreshUsersList() is a function that display which user are currently subscribed to the current channel
// if the user never send a message, then there is no username linked to the peer ID
// so we display a random username
// if the user already send a message, then we know what his username is. So we lookup the peer ID
// in the localStorage to retreive his username
// This function is called every 2 secondsto update this list if someone connect or deconnect
function refreshUsersList(){
  node.pubsub.peers(getCurrentChnl(), (err, peerIds) => {
    var peers;
    if (err) {
      return console.error(`failed to get peers subscribed to ${currentTab}`, err);
    }
    peers=peerIds;
    $('#usersList li').remove();
    for (i=0; i<peers.length;i++){
      var un=localStorage.getItem(peers[i]);
      if(un){
        $('#usersList').append('<li>'+un+'</li>');
      }
      else{
        localStorage.setItem(peers[i], randomEl(animaux)+' Anonyme');
      }
    }
  });
}

// refreshConnectedUser() is a function to display how many user are subscribed
// to a channel listed on the left column. This function is called every 2 seconds to update this number
function refreshConnectedUser(){
  $('#list-tab a').each(function(){
    var chl= $(this).attr("aria-label");
    node.pubsub.peers(chl, (err, peerIds) => {
      var peers;
      if (err) {
        return console.error(`failed to get peers subscribed to ${currentTab}`, err);
      }
      peers=peerIds;
      var nb=peers.length;
      $(this).find('span').text(nb+' Connected');
    });
  })
}

// function to pick up a random element in a list.
// we use it to select a random animal in the animal list and generate a random username
// as google do for Google Docs for example
function randomEl(list) {
  var i = Math.floor(Math.random() * list.length);
  return list[i];
}

var animaux = ["Addax","Agneau","Agouti","Aigle","Albatros","Alligator","Alpaga","Anaconda","Ane","Anoa","Antilope","Araignée","Atèle","Autruche","Aye-aye","Babouin","Baleine","Belette","Béluga","Biche","Bison","Blaireau","Blobfish","Boa","Boeuf","Bongos","Bonobo","Bouquetin","Brebis","Buffle","Cachalot","Campagnol","Canard","Capucin","Capybara","Caracal","Carcajou","Caribou","Carpe","Castor","Cerf","Chacal","Chameau","Chamois","Chat","Chaus","Chauve-souris","Cheval","Chèvre","Chevreuil","Chien","Chimpanzé","Chinchilla","Chouette","Cigogne","Civette","Coati","Cobaye","Cobe","Coccinelle","Cochon","Colocolo","Coq","Couscous","Coyote","Crabe","Crocodile","Cygne","Daim","Dauphin","Dinde","Dindon","Dingo","Drill","Dromadaire","Échidné","Écureuil","Élan","Éland","Éléphant","Émeu","Epaulard","Escargot","Espadon","Eyra","Faisan","Faon","Faucon","Fennec","Flamant","rose","Fouine","Fourmilier","Furet","Gaufre","Gaur","Gazelle","Gélada","Genette","Gerbille","Gerboise","Gibbon","Girafe","Glouton","Gnou","Goret","Gorille","Grenouille","Grizzly","Grue","Guanaco","Guépard","Guigna","Hamster","Hérisson","Hermine","Héron","Hibou","Hippocampe","Hippopotame","Hirondelle","Hyène","Ibis","Iguane","Impala","Isard","Isatis","Jaguar","Jaguarondi","Kangourou","Kiwi","Koala","Kodiak","Kodkod","Koudou","Lamantin","Lamas","Lapin","Lemming","Lémurien","Léopard","Lérot","Lézard","Lièvre","Lion","Loir","Lophophore","Lori","Loup","Loutre","Lycaons","Lynx","Macaque","Mammouth","Manchot","Mandrill","Mangouste","Manul","Mara","Marmotte","Marsouin","Martre","Mésange","Morse","Mouette","Moufette","Mouflon","Mouton","Mulet","Mulot","Musaraigne","Muscardin","Naja","Nandou","Narval","Nasique","Nason","Noctule","Notou","Numbat","Ocelot","Octodon","Oie","Okapi","Once","Opossum","Oran","Ornithorynque","Orques","Oryctérope","Oryx","Otarie","Ouistiti","Ours","Pandas","Pangolin","Panthère","Paon","Paresseux","Pécaris","Pékan","Pélican","Perroquet","Phacochère","Phoque","Pie","Pika","Pingouin","Pipistrelles","Piton","Pogona","Poisson","Polatouche","Poney","Porc","Porc-épic","Poule","Poulet","Poulpe","Poussin","Puma","Putois","Python","Quetzal","Quiscale","Quokka","Ragondin","Rat","Raton","laveur","Ratufa","Renard","Requin","Requin","baleine","Rhésus","Rhinocéros","Roussette","Salamandre","Sanglier","Serpent","Serval","Singe","Souris","Sphynx","Springbok","Suricate","Tamandua","Tamanoir","Tamarin","Tamia","Tapir","Tarsier","Tatou","Taupe","Taureau","Tigre","Tortue","Toucan","Trigonocéphale","Unau","Urubu","Vache","Varan","Vautour","Veau","Vipère","Vison","Wallaby","Wapiti","Watussi","Wombat","Xérus","Yack","Zèbre","Zébrule","Zébu","Zibeline","Zorille"];

// function to save a string to the clipboard of the client
// str is a argument passed as a string and contain what to save in the clipboard
// we use this function to save the token  when a private channel is created
function copyStringToClipboard (str) {
  var el = document.createElement('textarea');
  el.value = str;
  el.setAttribute('readonly', '');
  el.style = {position: 'absolute', left: '-9999px'};
  document.body.appendChild(el);
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
}

// function to update the frontend when the token is sas been copied to the clipboard
// and the modal is closed
function copyTkButton(){
  $('#chnlmodal').modal('hide');
  $("#createC").show();
  $("#copyToken").hide();
  copyStringToClipboard($("#tokenShow").val());
  $("#CreateCnlModalTitle").text("Enter the channel name");
}

// autocollaps the Conversation pannel when the window is mobile sized
$(window).on('resize', function(){
  var win = $(this);
  if (win.width() <= 767) {
    $("#list-tab").collapse('hide');
  }
  else{
    $("#list-tab").collapse('show');
  }

});

// this function change the first css of the index.html with the one passed in parametre
// it is used to switch between light and dark mode
function changeCSS(cssFile) {
  var oldlink = document.getElementsByTagName("link").item(0);
  oldlink.href = cssFile;
}

// function called when the h1 title of the pag is clicked
// it switch between light and dark mode by replacing the main css with another one
function switchDarkMode(){
  if($("link[href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css']").length){
    changeCSS('css/bootstrap.dark.css');
    localStorage.setItem("theme","dark");
  }
  else{
    changeCSS('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
    localStorage.setItem("theme","light");
  }
}

// this function use a JS library (crypto-js) to crypt strings using a custom key
// the first argument is the msg to crypt as a String
// the second argument is the key to use to crypt the message
// we salt the key to avoid all lucky decrypting
// the function return a crypted string
function chiffrerMsg(msg, key){
  return CryptoJS.AES.encrypt(msg, "RICM_CRYPTED-"+key).toString();
}

// this function use also the JS library (crypto-js) to decrypt a string using a key
// the first argument is the msg to decrypt as a String
// the second argument is the key to use to decrypt the message
// we use the same salt as the one when crypting our message of our application
// the function return a decrypted string
function decryptMsg(msg, key){
  return CryptoJS.AES.decrypt(msg, "RICM_CRYPTED-"+key).toString(CryptoJS.enc.Utf8);
}


// this function handle the file uploaded by the user before sending it to IPFS
// First the file opened with a reader as a DataURL (so Base64)
// Then, when the data has been processed, we send it to IPFS with node.add
// The Base64 plain information is directly send to IPFS. Not the file itself
// The hash of the uploaded file is the send to the channel with the file tag ([0010])
// as well as the file name and size
function sendFile() {
  var elem = document.querySelector('input[type=file]')
  var file = elem.files[0];
  console.log(file);
  var reader  = new FileReader();

  reader.addEventListener("load", function () {
    node.add(node.types.Buffer.from(reader.result), (err, files) => {
      if (err) return console.error(err)
      un=$("#usern").text();
      var msg = "[0010];"+file.name+";"+file.size+";"+ un + ";" +files[0].hash;
      pub(msg, getCurrentChnl());
      // msg = "[0001] - " + un + " uploaded a file : "+file.name+" -";
    })
  }, false);

  if (file) {
    reader.readAsDataURL(file);
  }
  elem.value = "";
}


// This function is the handler for message received with the file tag
// First we parse the message to separate all the data of the file in an array
// Then we add the file name + size + user to the file list of the according channel
// The data is not directly stored in the href (in order to download the file)
// we store ipfs hash as a function attribute (loadFromHash()) so that when the user click
// on the file link, it get the data from ipfs and then download the file to the user computer
// we also print a message in the chat when a file is received
function imageReceived(data){
  var preview = document.querySelector('img');
  var crypted = data.data.toString();
  var chl = data.topicIDs[0];
  var str = decryptMsg(crypted,chl);

  var strArr = str.split(";");
  var fileName = strArr[1];
  var fileSize = (parseInt(strArr[2], 10)/1000).toFixed(0);
  var usern = strArr[3];
  var hash = strArr[4];

  // console.log(str);
  var elem = document.createElement("li");
  elem.classList.add("list-group-item");
  var link = document.createElement("a");
  elem.appendChild(link);
  link.id=hash;
  link.href="javascript:loadFromHash('"+hash+"');"
  link.innerHTML="["+usern+"] "+fileName+" : "+fileSize+" Ko";
  link.download=fileName;
  $('#'+chl+'-filelist').append(elem);

  var msg = "-- " + usern + " uploaded a file : "+fileName+" --";
  printChat(chl,msg);
}

// cat a data from IPFS using a hash
// This function is used when the user click on a link to download a file
// First we get the Base64 data, and then we place it it the href attribute of the a link
// in order to download it as a single file (and not a plain data)
function loadFromHash(hash){
  node.cat(hash, (err, data) => {
    if (err) return console.error(err)
    $("#"+hash).attr("href","data:application/octet-stream;charset=utf-16le;"+data.toString());
    $("#"+hash)[0].click();
  })
}

// This function reload the file list according to the current opened channel
// It simply refresh the ui to hide the link of the files sent to other channel
// and display the right ones
function displayFileList(channel){
  $('#files ul').each(function() {
    $(this).hide();
  });
  if(!$('#'+channel+"-filelist").length){
    $('#file-list').append("<div><ul class='list-group list-group-flush' id='"+channel+"-filelist'></ul></div>")
  }
  else{
    $('#'+channel+"-filelist").show();
  }
}
