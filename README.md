# RICMessenger

RICMessenger is a decentralized chat application based on IPFS.

![Interface](screenshot.png)

## Description

RICMessenger is built on top of the IPFS network, using the pubsub feature. It is a decentralized and serverless application which can be run from any browser.

### Features

* Connect to private and public channels
* Send messages
* List of connected users on every channels
* Use multiple channels at the same time

### Users

In this application, users are represented by an IPFS node, which is initially created by the broswer. This node is persistent, so a user always interact with the application from the same node.

### Channels

There are two types of channels :
* Public channels : They are accessible to every users, they can join them by providing the channel name
* Private channels : These channels have a name and an associated token. To access this channel a user needs to provide a the channel name and the token (which is a shared secret between users of this channel)

### Messages

Messages are crypted to ensure that they can't be read outside of the application.
We also implemented several type of messages to perform differents tasks, which are : sending a message, and to send a file.


### Files 

As well as messages, users can send files. These files are uploaded to the IPFS network, allowing other user to retrieve them from the network using the file hash.

## Usage

This application is decentralized and serverless it can be run by any user from his browser by opening the index.html file. The whole project directory is necessary for the application to work.

## Mobile Application

It is possible to build an apk of our project using Apache Cordova as it is a javascript project.
